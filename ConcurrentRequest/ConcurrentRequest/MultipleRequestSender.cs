﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Net;

namespace ConcurrentRequest
{
    class MultipleRequestSender
    {
        public Task[] Tasks { get; set; }
        private  IRestClient client;
        public List<IRestRequest> RestRequests { get; set; }
   

        public MultipleRequestSender(string baseUrl)
        {
            this.client =new RestClient(baseUrl);
            
            RestRequests = new List<IRestRequest>();
        }

        public void LoadRequests(string source, Method method, List<object> requestObjects)
        {
            var restRequests = new List<IRestRequest>();

            foreach (var request in requestObjects)
            {
                restRequests.Add(new RestRequest(source, method).AddJsonBody(request));
            }

            RestRequests.AddRange(restRequests);
        }

        public async Task ExecuteAsync()
        {
             Tasks = RestRequests.Select(r => client.ExecuteTaskAsync(r)).ToArray();
              await Task.WhenAll(Tasks);
            
        }

        public async Task SendAllRequests()
        {

            await ExecuteAsync();

            var responses = Tasks.Select(t => (dynamic)t);

            while (responses.Where(r => r.Result.StatusCode == HttpStatusCode.ServiceUnavailable).Any()) 
            {
               var leftRequests = responses.Where(r => r.Result.StatusCode == HttpStatusCode.ServiceUnavailable)
                                            .Select(r => (IRestRequest)r.Result.Request).ToList();
                RestRequests = leftRequests;
                await ExecuteAsync();
                responses = Tasks.Select(t => (dynamic)t);
            }
           
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Threading;
using System.Net;
using System.Diagnostics;

namespace ConcurrentRequest
{
    class Program
    {
        
        static void Main(string[] args)
        {
            #region "Test"
            //var client = new RestClient("http://localhost/RestApi/api/");
            //client.Execute(new RestRequest("/products", Method.DELETE));
            //var taskList = new List<Task>();

            //Func<IRestRequest> newPostRequest = () => new RestRequest("/products", Method.POST)
            //                                  .AddJsonBody(new { Id = Guid.NewGuid(), Name = "Product" });


            //for (var i=0;i<10; i++)
            //{
            //    var request = newPostRequest();
            //    taskList.Add(client.ExecuteTaskAsync(request));

            //}

            //Task[] taskArray = taskList.ToArray();

            //WhenAllTasks(taskArray,client);
            #endregion
            MultipleRequestSender sender = new MultipleRequestSender("http://localhost/RestApi/api/");

            var productList = new List<object>();
            for (int i = 0; i < 5000; i++)
            {
                productList.Add(new { Id = Guid.NewGuid(), Name = "Product" });
            }

            sender.LoadRequests("/products", Method.POST, productList);
            sender.SendAllRequests();

            Console.ReadKey();
        }

        static async Task WhenAllTasks(Task[] tasks,RestClient client)
        {
            //Stopwatch stopWatch = new Stopwatch();
            //int successCount, badRequest, serviceUnavailable = 0;
            
            //stopWatch.Start();
            //await Task.WhenAll(tasks);
            //stopWatch.Stop();
            //var getAll = client.Execute(new RestRequest("/products", Method.GET)).Content;
            //var responses = tasks.Select(t => (dynamic)t);

            //successCount = responses.Count(t => t.Result.StatusCode == HttpStatusCode.OK);
            //badRequest = responses.Count(t => t.Result.StatusCode == HttpStatusCode.BadRequest);
            //serviceUnavailable = responses.Count(t => t.Result.StatusCode == HttpStatusCode.ServiceUnavailable);

            //var otherResponses = responses.Where(t => t.Result.StatusCode != HttpStatusCode.OK).Select(t=> t.Result).ToList();

            //Console.WriteLine("Sent Requests = {0} \nSuccesful Responses = {1} \nBad Requests = {2} \nService Unavailable Error = {3} \nCompleted in {4} ms. \nGET ALL: \n{5}", tasks.Length, successCount,badRequest,serviceUnavailable, stopWatch.ElapsedMilliseconds.ToString(),getAll);
           
        }


    }
}
